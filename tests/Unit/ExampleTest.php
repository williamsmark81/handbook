<?php

namespace Tests\Unit;

use Tests\TestCase;
use Tests\ExampleTesting;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }

}


class IndexTest extends TestCase
{
  public function testingInstall()
  {
    $this->visit('/')
        ->see('Laravel');
  }
}
