<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laracasts\Integrated\Extensions\Laravel as IntegrationTest;

class TestCase extends IntegrationTest {
  protected $baseUrl = 'http://localhost:8000';
}
// 
// abstract class TestCase extends BaseTestCase
// {
//     use CreatesApplication;
// }
