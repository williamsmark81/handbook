<?php
$I = new FunctionalTester($scenario);
$I->wantTo('Test Landing Page Loads');

$I->amOnPage('/');
$I->see('EDGEHILL UNIVERSITY MODULE HANDBOOKS');

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

$I = new FunctionalTester($scenario);
$I->wantTo('Login');

$I->amOnPage('/login');
$I->see('Login');
$I->fillField('email', 'chris@example.com');
$I->fillField('password', 'webdev');
$I->click('Login');
$I->amOnPage('/');

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

$I = new FunctionalTester($scenario);
$I->wantTo('Add New Module');

$I->amOnPage('modules/create');
$I->see('Create New Module');
$I->fillField('department', 'Business');
$I->fillField('ModuleID', 'BUS3212');
$I->fillField('moduleName','Business Test');
$I->click('Submit');
$I->amOnPage('departments');
