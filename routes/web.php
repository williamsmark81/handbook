<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Module;

Route::get('/', function () {
    return view('welcome');
})->name('home');
/* ====================================================================
AUTH ROUTES
==================================================================== */

Route::get('register', 'RegistrationController@create');
Route::post('register', 'RegistrationController@store');
Route::get('login', 'SessionsController@create');
Route::get('logout', 'SessionsController@destroy');


/* ====================================================================
MODULES ROUTES
==================================================================== */

Route::get('modules', 'ModulesController@index');

// Route::get('new/modules', 'ModulesController@create');
Route::get('modules/create', 'ModulesController@create');


Route::post('modules', 'ModulesController@store');

Route::get('modules/handbook', 'HandbookController@index');
Route::get('modules/handbook/create', 'HandbookController@create')->middleware('auth');
Route::get('modules/{module}/handbook', 'HandbookController@show');
Route::post('modules/handbook', 'HandbookController@store');

Route::get('modules/{module}', 'ModulesController@show');


/* ====================================================================
DEPARTMENT ROUTES
==================================================================== */

Route::get('department', 'DepartmentsController@index');
Route::get('dept', function () {
    return redirect('department');
});
Route::get('departments', function () {
    return redirect('department');
});

Route::get('department/{depts}', 'DepartmentsController@show');

/* ====================================================================
GENERIC ROUTES
==================================================================== */

Route::get('computing', function () {
  return view('computing');
});

Route::get('media', function () {
  return view('media');
});

Route::get('business', function () {
  return view('business');
});

// Auth::routes();

// Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');
