<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          array('name' => 'Alan Turing', 'email' => 'alan@example.com', 'password' => bcrypt('webdev')),
          array('name' => 'Barry Tickle', 'email' => 'barry@example.com', 'password' => bcrypt('webdev')),
          array('name' => 'Clare Spalding', 'email' => 'clare@example.com', 'password' => bcrypt('webdev')),

        ]);
    }
}
