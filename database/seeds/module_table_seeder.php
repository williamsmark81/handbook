<?php

use Illuminate\Database\Seeder;

class module_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
          array('module_id' => 'CIS3301', 'module_name' => 'Module Zero One', 'department_id' => 1),
          array('module_id' => 'SPY1111', 'module_name' => 'Module Eleven', 'department_id' => 3),
          array('module_id' => 'BUS1014', 'module_name' => 'Module Fourteen', 'department_id' => 2)
        ]);
    }
}
