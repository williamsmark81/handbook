<?php

use Illuminate\Database\Seeder;

class ExtensionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('extensions')->insert([
        array('body' => 'f you are unable to submit to the stated deadline, you should (where possible) contact your module tutor prior to the submission deadline.  All requests for extension will be considered by the Director of Undergraduate Studies.

Certificate of Extension
 Only in extreme cases will extensions be granted for coursework. 

Student guidelines for applying for an extension, a separate form must be completed for each piece of coursework for which you are applying for an extension.
Any application for an extension of a deadline should normally be made at least twenty-four hours before the due deadline for the assessment.
Extensions may be granted for a period that extends up to two weeks before an Assessment Board. If a period longer than this is required you may be advised to apply for consideration under exceptional mitigating circumstances.
All applications for an extension should, wherever possible, be accompanied by documentary evidence.')
      ]);
    }
}
