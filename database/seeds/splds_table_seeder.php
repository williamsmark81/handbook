<?php

use Illuminate\Database\Seeder;

class splds_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('splds')->insert([
          'email' => 'spld@edgehill.ac.uk',
          'url' => 'http://www.ehu.ac.uk/spld',
          'phone' => '01695584372',
          'header' => 'Specific Learning Difficulties',
          'body' => 'The SpLD Support Team at Edge Hill has a range of tailored and specialised services for students with Specific Learning Difficulties. Specific Learning Difficulty is a general term used to refer to a number of persistent difficulties that impact upon learning. Specific Learning Difficulties include:

            Dyslexia is a pattern of difficulties relating to language and can affect memory, reading, sequencing, spelling, organisation, automaticity and speed of information processing.

Dyspraxia (also known as Developmental Co-ordination Disorder) is an impairment or immaturity of the organisation of movement. (Dyspraxia Foundation). Dyspraxic difficulties can affect memory, concentration, handwriting, planning and organisation, co-ordination and spatial awareness.

Dysgraphia difficulties in the grapho-motor domain (handwriting), which may be slow and impact upon quality and / or rate of output.

Dyscalculia difficulty involving the most basic aspects of arithmetical skills.

Attention Deficit [Hyperactivity] Disorder (ADD / ADHD) exists with or without hyperactivity and encompasses difficulties with the control of impulses and behaviour, concentration and attention span.

SpLDs can range from mild to severe and are not related to intelligence. There is a considerable overlap between the characteristics of SpLDs and a person can have indicators of more than one of the above.'
        ]);
    }
}
