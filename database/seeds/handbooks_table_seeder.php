<?php

use Illuminate\Database\Seeder;

class handbooks_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('handbooks')->insert([
          array('accesses_id' => '1', 'department_id' => 1, 'module_id' => 1, 'inclusion_id' => 1, 'malpractice_id' => 1, 'extension_id' => 1, 'attendance_id' => 1, 'spld_id' => 1, 'module_overview' => 'test input for module overview', 'course_overview' => 'test content for course overview', 'learning_outcomes' => 'Test content for learning outcomes', 'transferable_skills' => 'Test content for transferable skills', 'assessment' => 'Test content for assessments'),
          array('accesses_id' => '1', 'department_id' => 3, 'module_id' => 2, 'inclusion_id' => 1, 'malpractice_id' => 1, 'extension_id' => 1, 'attendance_id' => 1, 'spld_id' => 1, 'module_overview' => 'test input for module overview', 'course_overview' => 'test content for course overview', 'learning_outcomes' => 'Test content for learning outcomes', 'transferable_skills' => 'Test content for transferable skills', 'assessment' => 'Test content for assessments'),
          array('accesses_id' => '1', 'department_id' => 2, 'module_id' => 3, 'inclusion_id' => 1, 'malpractice_id' => 1, 'extension_id' => 1, 'attendance_id' => 1, 'spld_id' => 1, 'module_overview' => 'test input for module overview', 'course_overview' => 'test content for course overview', 'learning_outcomes' => 'Test content for learning outcomes', 'transferable_skills' => 'Test content for transferable skills', 'assessment' => 'Test content for assessments')
        ]);
    }
}
