<?php

use Illuminate\Database\Seeder;

class InclusionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inclusions')->insert([
          array('intro'=>'test', 'list'=>'test test', 'contact'=>'testy mctestface', 'email'=>'text@example.com', 'phone'=>'01695123456', 'location'=>'test location',)
        ]);
    }
}
