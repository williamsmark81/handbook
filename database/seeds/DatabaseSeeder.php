<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // $this->call(UsersTableSeeder::class);
        // $this->call(AccessesTableSeeder::class);
        // $this->call(AttendancesTableSeeder::class);
        // $this->call(departments_table_seeder::class);
        // $this->call(ExtensionsTableSeeder::class);
        // $this->call(InclusionsTableSeeder::class);
        // $this->call(MalpracticesTableSeeder::class);
        // $this->call(module_table_seeder::class);
        // $this->call(splds_table_seeder::class);
        $this->call(handbooks_table_seeder::class);
        $this->call(TimetableTableSeeder::class);
        $this->call(ReadingsTableSeeder::class);
     }
}
