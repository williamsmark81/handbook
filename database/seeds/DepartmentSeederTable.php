<?php

use Illuminate\Database\Seeder;

class DepartmentSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('modules')->insert([
        array('department_name' => 'Computer-Science'),
        array('department_name' => 'Social-Science'),
        array('department_name' => 'Business'),
        array('department_name' => 'Law-and-Criminology')
      ]);
    }
}
