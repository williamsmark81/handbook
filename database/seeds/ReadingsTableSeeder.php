<?php

use Illuminate\Database\Seeder;

class ReadingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('readings')->insert([
        array('author' => 'Peter Staker', 'title' => 'Hot Fuzz', 'description' => 'some random bio about why you should read it', 'handbook_id' => '1'),
        array('author' => 'Ian Flemming', 'title' => 'Thunderball', 'description' => 'Cant beat a bit of Bond', 'handbook_id' => '1'),
        array('author' => 'Peter Staker', 'title' => 'Hot Fuzz', 'description' => 'some random bio about why you should read it', 'handbook_id' => '2'),
        array('author' => 'Ian Flemming', 'title' => 'Thunderball', 'description' => 'Cant beat a bit of Bond', 'handbook_id' => '2'),
        array('author' => 'Peter Staker', 'title' => 'Hot Fuzz', 'description' => 'some random bio about why you should read it', 'handbook_id' => '3'),
        array('author' => 'Ian Flemming', 'title' => 'Thunderball', 'description' => 'Cant beat a bit of Bond', 'handbook_id' => '3'),
      ]);
    }
}
