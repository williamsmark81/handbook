<?php

use Illuminate\Database\Seeder;

class MalpracticesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('malpractices')->insert([
        array('body'=>'  malpractice test')
      ]);
    }
}
