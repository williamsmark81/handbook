<?php

use Illuminate\Database\Seeder;

class departments_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
          array('department_name' => 'Computer-Science'),
          array('department_name' => 'Business'),
          array('department_name' => 'Social-Science'),
          array('Department_name' => 'Law-and-Criminology')
        ]);
    }
}
