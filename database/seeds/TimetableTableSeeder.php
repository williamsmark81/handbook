<?php

use Illuminate\Database\Seeder;

class TimetableTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('timetables')->insert([
        array('week'=>'one', 'body' => 'week one timetable', 'handbook_id' => '1'),
        array('week'=>'two', 'body' => 'week two timetable', 'handbook_id' => '1'),
        array('week'=>'one', 'body' => 'week one timetable', 'handbook_id' => '2'),
        array('week'=>'two', 'body' => 'week two timetable', 'handbook_id' => '2'),
        array('week'=>'one', 'body' => 'week one timetable', 'handbook_id' => '3'),
        array('week'=>'two', 'body' => 'week two timetable', 'handbook_id' => '3'),
      ]);
    }
}
