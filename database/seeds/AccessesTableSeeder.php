<?php

use Illuminate\Database\Seeder;

class AccessesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('accesses')->insert([
        array('status' => 'user'),
        array('status' => 'lecturer'),
        array('status' => 'leader'),
        array('status' => 'admin')
      ]);
    }
}
