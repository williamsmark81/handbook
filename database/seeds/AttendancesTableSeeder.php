<?php

use Illuminate\Database\Seeder;

class AttendancesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('attendances')->insert([
        array('body' => '
        Mode of attendance
        Lecture

        The substantive content of the module will be explored and discussed through an integrated combination of lecturing, seminar-based discussion, and practical exercises. Concepts will first be covered theoretically through formal lectures and issues arising from these discussed in a seminar-like setting. The practical impact of the concepts will then be investigated and the conceptual understanding deepened through series of computer-based exercises.

        Support:
        Support will be offered within the formal contact sessions, in particular in monitoring the practical aspect. Students who have access to electronic mail will be encouraged to make use of this to communicate within their group and where necessary to send work to the tutor for comment, or to seek additional guidance.

        Attendance, Monitoring and Tracking Processes
        Attendance at all lectures, seminars, and lab sessions is compulsory. If you fail to attend sessions, you will be contacted requesting an explanation for your absence.  Persistent non-attendance will trigger a formal student review meeting with the Director of Studies, and in exceptional cases can lead to you being withdrawn from the course.

        Consequently, if you are unable to attend any of your timetabled sessions you must let your tutor and the Department of Computing Administrators know as soon as possible. You should also complete an Absence Notification Form for each session missed and leave it in the box provided in the reception area of the Department of Computing. Additional documentation such as medical certificates can also be attached. Tutors will take this evidence into account when monitoring attendance and tracking.

        You are advised to consult your Personal Tutor if there is a problem leading to long-term absence (or a pattern of shorter absences) from the University. It is your responsibility to catch-up with academic work missed during a period of absence. DO NOT assume that your absence from the programme is approved. The University reserves the right to reject requests for absence, including self-certification of sickness, if there are concerns about a student’s overall pattern of attendance. 
        ')
      ]);
    }
}
