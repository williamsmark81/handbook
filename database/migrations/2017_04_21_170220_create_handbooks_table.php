<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHandbooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('handbooks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('department_id')->unsigned();
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');
            $table->integer('module_id')->unsigned();
            $table->foreign('module_id')->references('id')->on('modules')->onDelete('cascade');
            // $table->text('student_learning');
            $table->integer('inclusion_id')->unsigned();
            $table->foreign('inclusion_id')->references('id')->on('inclusions')->onDelete('cascade');
            $table->integer('malpractice_id')->unsigned();
            $table->foreign('malpractice_id')->references('id')->on('malpractices')->onDelete('cascade');
            $table->integer('extension_id')->unsigned();
            $table->foreign('extension_id')->references('id')->on('extensions')->onDelete('cascade');
            // $table->integer('reading_id')->unsigned();
            // $table->foreign('reading_id')->references('id')->on('readings')->onDelete('cascade');
            $table->integer('attendance_id')->unsigned();
            $table->foreign('attendance_id')->references('id')->on('attendances')->onDelete('cascade');
            $table->integer('spld_id')->unsigned();
            $table->foreign('spld_id')->references('id')->on('splds')->onDelete('cascade');
            // $table->integer('accesses_id')->unsigned();
            // $table->foreign('accesses_id')->references('id')->on('accesses')->onDelete('cascade');
            $table->text('module_overview');
            $table->text('course_overview');
            $table->text('learning_outcomes');
            $table->text('transferable_skills');
            $table->text('assessment');
            $table->timestamps();
        });

        Schema::create('handbook_user', function (Blueprint $table) {
          $table->integer('handbook_id')->unsigned();
          $table->foreign('handbook_id')->references('id')->on('handbooks')->onDelete('cascade');
          $table->integer('user_id')->unsigned();
          $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('handbooks');
        Schema::dropIfExists('handbook_user');
    }
}
