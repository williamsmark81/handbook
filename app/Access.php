<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Access extends Model
{
    public function handbooks()
    {
      return $this->hasOne(Handbook::class);
    }
}
