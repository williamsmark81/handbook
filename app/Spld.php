<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spld extends Model
{
  public function handbooks()
  {
    return $this->belongsTo(Handbook::class);
  }
}
