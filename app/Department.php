<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public function modules()
    {
      return $this->hasMany(Module::class);
    }
}
