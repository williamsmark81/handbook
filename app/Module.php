<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public function departments()
    {
      return $this->belongsTo(Department::class);
    }

    public function handbooks()
    {
      return $this->hasOne(Handbook::class);
    }
}
