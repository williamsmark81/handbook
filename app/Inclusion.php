<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inclusion extends Model
{
    public function handbooks()
    {
      return $this->blongsTo(Handbook::class);
    }
}
