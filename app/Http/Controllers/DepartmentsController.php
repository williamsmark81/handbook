<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;

class DepartmentsController extends Controller
{
  public function index()
  {
    $depts = Department::all();
    return view('departments.index', compact('depts'));
  }

  public function show($id)
  {
    $dept = Department::where('department_name', $id)->firstOrFail();
    return view('departments.show', compact('dept'));
  }
}
