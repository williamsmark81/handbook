<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Module;
use App\Department;
use App\Post;
use App\Handbook;

class ModulesController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth')->except(['index', 'show']);
  }

    public function index()
    {
      $mods = Module::all();
      return view('modules.index', compact('mods'));
    }

    public function show($id)
    {
      $mod = Module::where('module_id', $id)->first();
      $book = Handbook::where('module_id', $mod->id)->first();
      return view('modules.show', compact('mod', 'book'));
    }

    public function create()
    {
      $depts = Department::all();
      return view('modules.create', compact('depts'));
    }

    public function store()
    {
      // validate data. Regex ensures 3 uppercase letters followed by 4 numbers only.
      $this->validate(request(), [
        'moduleID' => 'required|min:7|max:7|unique:modules,module_id|regex:/[A-Z]{3,}[0-9]{4,}/',
        'moduleName' => 'required',
        'department' => ''
      ]);

      // dd(request()->all());

      // New module class to submit data
      $submit = new Module;

      // Database entry = form name
      $submit->module_id = request('moduleID');;
      $submit->module_name = request('moduleName');
      $submit->department_id = request('department');

      // Save data to database
      $submit->save();

      // Redirect to page when submitted
      return redirect('department');
    }
}
