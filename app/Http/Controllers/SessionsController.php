<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionsController extends Controller
{
  public function index()
  {

  }

  public function create()
  {
    return view('sessions.create');
  }

  public function destroy()
  {
    // Logs user out
    auth()->logout();

    // Redirect user to home page
    return redirect()->home();
  }
}
