<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\Module;
use App\Handbook;
use App\Spld;
use App\Attendance;
use App\Extension;
use App\Inclusion;
use App\Malpractice;
use App\Reading;
use App\Timetable;

class HandbookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mods = Module::all();
        return view('handbooks.index', compact('mods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $depts = Department::all();
        $mods = Module::all();
        return view('handbooks.create', compact('depts', 'mods'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // validate data. Regex ensures 3 uppercase letters followed by 4 numbers only.
      $this->validate(request(), [
        'module_id' => 'required|min:7|max:7|unique:modules,module_id|regex:/[A-Z]{3,}[0-9]{4,}/',
        // 'department_id' => '',
        'moduleName' => 'required|min:3|max:255unique:modules,moduleName',
        'department' => 'required|max:1',
        'module_overview' => 'required|min:3',
        'course_overview' => 'required|min:3',
        'learning_outcomes' => 'required|min:3',
        'assessment' => 'required|min:3',
        'transferable_skills' => 'required|min:3'
      ]);

      // dd(request()->all());

      $add = new Module;

      $add->module_id = request('module_id');
      $add->department_id = request('department');
      $add->module_name = request('moduleName');
      $add->save();

      $last = Module::latest()->first();

      // New module class to submit data
      $submit = new Handbook;

      // $submit->department_id = request('department');
      $submit->module_overview = request('module_overview');
      $submit->course_overview = request('course_overview');
      $submit->learning_outcomes = request('learning_outcomes');
      $submit->assessment = request('assessment');
      $submit->transferable_skills = request('transferable_skills');
      $submit->module_id = $last->id;
      $submit->department_id = $last->department_id;
      $submit->inclusion_id = 1;
      $submit->malpractice_id = 1;
      $submit->extension_id = 1;
      $submit->attendance_id =1;
      $submit->spld_id = 1;
      $submit->accesses_id = 1;

      // Save data to database
      $submit->save();

      $depts = Department::all();
      return view('departments.index', compact('depts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $attends = Attendance::first();
        $exts = Extension::first();
        $incs = Inclusion::first();
        $malps = Malpractice::first();
        $splds = Spld::first();
        $mod = Module::where('module_id', $id)->first();
        $book = Handbook::where('module_id', $mod->id)->first();
        $reads = Reading::all()->where('handbook_id', $mod->id);

        return view('handbooks.show', compact('attends', 'exts', 'incs', 'malps', 'splds', 'mod', 'book', 'reads'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
