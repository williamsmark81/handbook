<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class RegistrationController extends Controller
{
    public function index()
    {

    }

    public function create()
    {

      return view('register.create');
    }

    public function store()
    {
      // Valid form

      $this->validate(request(), [

        'name' => 'required',
        'email' => 'required|email',
        'password' => 'required|confirmed'

      ]);

      // Create new user entry

      $user = User::create(request(['name', 'email', 'password']));

      // Auto sign in after RegistrationController

      \Auth::login($user);

      // Redirect

      return redirect()->home();

    }
}
