<?php

namespace App\Http\Controllers;

use App\Malpractice;
use Illuminate\Http\Request;

class MalpracticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Malpractice  $malpractice
     * @return \Illuminate\Http\Response
     */
    public function show(Malpractice $malpractice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Malpractice  $malpractice
     * @return \Illuminate\Http\Response
     */
    public function edit(Malpractice $malpractice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Malpractice  $malpractice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Malpractice $malpractice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Malpractice  $malpractice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Malpractice $malpractice)
    {
        //
    }
}
