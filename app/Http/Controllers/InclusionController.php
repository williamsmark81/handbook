<?php

namespace App\Http\Controllers;

use App\Inclusion;
use Illuminate\Http\Request;

class InclusionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inclusion  $inclusion
     * @return \Illuminate\Http\Response
     */
    public function show(Inclusion $inclusion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inclusion  $inclusion
     * @return \Illuminate\Http\Response
     */
    public function edit(Inclusion $inclusion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inclusion  $inclusion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inclusion $inclusion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inclusion  $inclusion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inclusion $inclusion)
    {
        //
    }
}
