<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Malpractice extends Model
{
    public function Handbooks()
    {
        return $this->belongsTo(Handbook::class);
    }
}
