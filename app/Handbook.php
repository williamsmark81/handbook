<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Handbook extends Model
{
    public function splds()
    {
      return $this->hasOne(Spld::class);
    }

    public function modules()
    {
      return $this->belongsTo(Module::class);
    }

    public function timetables()
    {
      return $this->belongsTo(Timetable::class);
    }

    public function readings() {
      return $this->hasMany(Reading::class);
    }

    public function malpractices()
    {
      return $this->hasOne(Malpractice::class);
    }

    public function inclusions()
    {
      return $this->hasOne(Inclusion::class);
    }

    public function extensions()
    {
      return $this->hasOne(Extension::class);
    }

    public function attendances()
    {
      return $this->hasOne(Attendance::class);
    }

    public function accesses() {
      return $this->belongsTo(Access::class);
    }
}
