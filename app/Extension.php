<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Extension extends Model
{
    public function handbooks()
    {
      return $this->belongsTo(Handbook::class);
    }
}
