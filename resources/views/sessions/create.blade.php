@extends('template.layout')

@section('content')

<h1>User Login</h1>

@if (Auth::check())
  Error! You are already logged in, do you want to <a href="{{ url('logout') }}">Logout {{ Auth::user()->name }}</a>
@else

<form class="form-horizontal" method="POST" action="/login">
  {{ csrf_field() }}
<fieldset>

<!-- Form Name -->
<legend>Login</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">email</label>
  <div class="col-md-4">
  <input id="email" name="email" type="text" placeholder="email" class="form-control input-md" required="">

  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="password">password</label>
  <div class="col-md-4">
    <input id="password" name="password" type="password" placeholder="password" class="form-control input-md" required="">

  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submit" class="btn btn-primary">Login</button>
  </div>
</div>

<div class="form-group">
  @include('partial.error')
</div>

</fieldset>
</form>

@endif

@endsection
