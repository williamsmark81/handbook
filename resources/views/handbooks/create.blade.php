@extends('template.layout')

@section('content')

<div class="flex-center position-ref full-height">

    <div class="content">
        <div class="title m-b-md">
            Create New Module
        </div>
        <form class="form-horizontal" method="POST" action="/modules/handbook">
          {{ csrf_field() }}
        <fieldset>

        <!-- Form Name -->
        <div class="content">
            <div class="title m-b-md">
                <legend>Create New Module</legend>
            </div>
        </div>

        <!-- Select Basic, Department list gathered and rendered from JSON -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="department">Department</label>
          <div class="col-md-4">
            <select id="department" name="department" class="form-control" required="">
              @foreach($depts as $dept)
                <option value="{{ $dept->id}}">{{ $dept->department_name }}</option>
              @endforeach
            </select>
          </div>
        </div>

        <!-- Textarea, String to save module ID, server auth 3 letters 4 numbers -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="module_id">Module id</label>
          <div class="col-md-4">
            <input id="module_id" name="module_id" type="text" placeholder="Module ID" class="form-control input-md">
          </div>
        </div>

        <!-- Textarea, String to save actual module name. -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="moduleName">Module Name</label>
          <div class="col-md-4">
            <input id="moduleName" name="moduleName" type="text" placeholder="Module name" class="form-control input-md">
          </div>
        </div>

        <!-- Textarea, text to save full module description / overview -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="module_overview">Module overview</label>
          <div class="col-md-4">
            <textarea class="form-control" id="module_overview" name="module_overview"></textarea>
          </div>
        </div>

        <!-- Textarea, text to save full course overview / description -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="course_overview">Course overview</label>
          <div class="col-md-4">
            <textarea class="form-control" id="course_overview" name="course_overview"></textarea>
          </div>
        </div>

        <!-- Textarea, text to save learning outcomes. module/handbook specific -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="learning_outcomes">Learning Outcomes</label>
          <div class="col-md-4">
            <textarea class="form-control" id="learning_outcomes" name="learning_outcomes"></textarea>
          </div>
        </div>

        <!-- Textarea, text to save additional transferable skills -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="transferable_skills">Transferable skills</label>
          <div class="col-md-4">
            <textarea class="form-control" id="transferable_skills" name="transferable_skills"></textarea>
          </div>
        </div>

        <!-- Textarea, text to save assessment information, formative, summative, weighting etc -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="assessment">Assessment</label>
          <div class="col-md-4">
            <textarea class="form-control" id="assessment" name="assessment"></textarea>
          </div>
        </div>

        <!-- Button -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="Submit"></label>
          <div class="col-md-4">
            <button id="Submit" name="Submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
          @include('partial.error')
        </fieldset>
        </form>


    </div>
</div>
@endsection
