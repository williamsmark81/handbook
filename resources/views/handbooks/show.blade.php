@extends('template.layout')

@section('content')

<div class="menu">
  <a href="#inclusion">Inclusion</a>
  <a href="#spld">SpLD</a>
  <a href="#attendance">Attendance</a>
  <a href="#extension">Extensions</a>
  <a href="#module_overview">Module Overview</a>
  <a href="#course_overview">Course Overview</a>
</div>

<article class="">

  <section id="inclusion">
    <h3>inclusion</h3>
    <p>
      {{ $incs->intro }}
    </p>
    <p>
      <strong>Contact</strong>: {{ $incs->contact }}
    </p>
    <p>
      <strong>Phone</strong>: {{ $incs->phone }}
    </p>
    <p>
      <strong>Email</strong>: {{ $incs->email }}
    </p>
    <p>
      Alternatively, come find us at {{ $incs->location}}
    </p>
  </section>

  <section id="spld">
    <h3>Specific Learning Difficulties (SpLD)</h3>
    <p>
      {{ $splds->intro }}
    </p>
    <p>
      <strong>Contact</strong>: {{ $splds->contact }}
    </p>
    <p>
      <strong>Phone</strong>: {{ $splds->phone }}
    </p>
    <p>
      <strong>Email</strong>: {{ $splds->email }}
    </p>
    <p>
      Alternatively, come find us at {{ $splds->location}}
    </p>
  </section>

  <section id="attendance">
    <h3>Attendance</h3>
    <p>
      {{ $attends->body }}
    </p>
  </section>

  <section id="Malpractice">
    <h3>Malpractice</h3>
    <p>
      {{ $malps->body }}
    </p>
  </section>

  <section id="extension">
    <h3>Extensions</h3>
    <p>
      {{ $exts->body }}
    </p>
  </section>

  <section id="module_overview">
    <h3>Module Overview</h3>
    <p>{{ $book->module_overview }}</p>
  </section>

  <section id="course_overview">
    <h3>Course Overview</h3>
    <p>{{ $book->course_overview}}</p>
  </section>



  <section>
    <h3>Suggested Reading</h3>
    <ul>
      @foreach ($reads as $read)
        <li><p>{{ $read->title }} by {{ $read->author }}</p>
        <p>{{ $read->description }}</p></li>
      @endforeach
    </ul>

  </section>



</article>

@endsection
