@extends('template.layout')

@section('content')

<div class="flex-center position-ref full-height">

  <div class="content">
    <div class="title m-b-md">
      Modules List
    </div>
    <ul>
      <li>{{ $spdl->header }}</li>
    </ul>
  </div>
</div>

@endsection
