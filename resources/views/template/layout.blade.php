<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="Mark williams">
  <meta name="description" content="A project that is designed to allow different different level user access to CRUD a specific module/degree handbook">
  <title>Handbook</title>
  <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css">

</head>
<body>
  @include('partial.nav')
  @yield('content')
  @include('partial.footer')
</body>
</html>
