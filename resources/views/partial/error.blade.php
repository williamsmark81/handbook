@if(count($errors))
  <div class="content flex-center">
      <ul class="alert">
        @foreach($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
  </div>
@endif
