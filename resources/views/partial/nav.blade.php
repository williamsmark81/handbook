<nav role="navigation" class="links flex-center">
    <a href="{{ url('/') }}">Home</a>
    <a href="{{ url('/departments') }}">Departments</a>
    <a href="{{ url('/modules') }}">Modules</a>
    @if (Auth::check())
    <a href="{{ route('logout') }}"
        onclick="event.preventDefault();
                 document.getElementById('logout-form').submit();">
        Logout {{ Auth::user()->name }}
    </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
      @else
      <a href="{{ url('register') }}"> Register</a>
      <a href="{{ url('login') }}"> Login</a>
    @endif
</nav>
