@extends('template.layout')

@section('content')

<div class="flex-center position-ref full-height">

    <div class="content">
        <div class="title m-b-md">
            Create New Module
        </div>
        <form class="form-horizontal" method="POST" action="/modules">
          {{ csrf_field() }}
        <fieldset>

        <!-- Form Name -->
        <div class="content">
            <div class="title m-b-md">
                <legend>Create New Module</legend>
            </div>
        </div>

        <!-- Select Basic -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="department">Department</label>
          <div class="col-md-4">
            <select id="department" name="department" class="form-control" required="">
              @foreach($depts as $dept)
                <option value="{{ $dept->id}}">{{ $dept->department_name }}</option>
              @endforeach
            </select>
          </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-4 control-label" for="moduleID">Module ID</label>
          <div class="col-md-4">
          <input id="moduleID" name="moduleID" type="text" placeholder="Module ID..." class="form-control input-md" required="">
          <div class="help">Help
            <span class="helptext">Module ID's should be a combination of 3 uppercase letters and 4 numbers, for example, ABC1234</span>
          </div>
          </div>

        </div>

        <!-- Text input-->
        <div class="form-group">
          <label class="col-md-4 control-label" for="moduleName">Module Name</label>
          <div class="col-md-4">
          <input id="moduleName" name="moduleName" type="text" placeholder="Module name..." class="form-control input-md" required="">
          </div>
        </div>


        <!-- Button -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="Submit"></label>
          <div class="col-md-4">
            <button id="Submit" name="Submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
          @include('partial.error')
        </fieldset>
        </form>


    </div>
</div>
@endsection
