@extends('template.layout')

@section('content')

<div class="flex-center position-ref full-height">


  <div class="content">
    <div class="title m-b-md">
      Modules List
    </div>
    <ul>
      @foreach ($mods as $mod)
        <li><a href="{{url('modules/' . $mod->module_id) }}">{{ $mod->module_name }}</a></li>
      @endforeach
    </ul>
  </div>
</div>

@endsection
