@extends('template.layout')

@section('content')

<div class="flex-center position-ref full-height">

    <div class="content flex-center column">
        <div class="title m-b-md">
          <img src="{{asset('/images/Crest.png')}}">
            {{ $mod->module_id }}
        </div>
        <p>Module: {{ $mod->module_name }}</p>
        <p>Module Overview: {{ $book->module_overview }}</p>
        <p>Course Overview: {{ $book->course_overview }}</p>
        <p>Learning Outcomes: {{ $book->learning_outcomes }}</p>
        <p>Assessment: {{ $book->assessment }}</p>
        <a href="{{ url('/modules/' . $mod->module_id . '/handbook')}}">View Handbook</a>
    </div>

</div>

@endsection
