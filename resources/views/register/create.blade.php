@extends('template.layout')

@section('content')

<h1>Register</h1>

<form class="form-horizontal" method="POST" action="/register">
  {{ csrf_field() }}
<fieldset>

<!-- Form Name -->
<legend>Form Name</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="name">name</label>
  <div class="col-md-4">
  <input id="name" name="name" type="text" placeholder="placenameholder" class="form-control input-md" required="">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">email</label>
  <div class="col-md-4">
  <input id="email" name="email" type="text" placeholder="email" class="form-control input-md" required="">

  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="password">password</label>
  <div class="col-md-4">
    <input id="password" name="password" type="password" placeholder="password" class="form-control input-md" required="">

  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="password_confirmation">password confirmation</label>
  <div class="col-md-4">
    <input id="password_confirmation" name="password_confirmation" type="password" placeholder="password" class="form-control input-md" required="">

  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submit" class="btn btn-primary">Register</button>
  </div>
</div>

<div class="form-group">
  @include('partial.error')
</div>

</fieldset>
</form>

@endsection
