@extends('template.layout')

@section('content')

<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @if (Auth::check())
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ url('/login') }}">Login</a>
                <a href="{{ url('/register') }}">Register</a>
            @endif
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            Departments Directory
        </div>

        <ul>
          @foreach ($depts as $dept)
            <li><a href="{{url('department/' . $dept->department_name) }}">{{ $dept->department_name }}</a></li>
          @endforeach
        </ul>
        
    </div>
</div>
@endsection
